# Coroutines

Execute coroutines anywhere.

## Installation

### Requirement

* Unity 2021.3 or later

### Using Unity Packager Manager

``` json
{
    "dependencies": {
        "com.quinmirn.coroutines": "https://gitlab.com/qupm/com.quinmirn.coroutines.git"
    }
}
```

## Usage

``` c#
 CoroutineManager.StartCoroutine(MyCoroutine());
```
