using System.Collections;
using UnityEngine;

namespace Quinmirn.Coroutines
{
    public static class CoroutineManager
    {
        private static CoroutineManagerInternal INSTANCE { get; }

        static CoroutineManager()
        {
            INSTANCE = new GameObject("[CoroutineManager]")
                .AddComponent<CoroutineManagerInternal>();

            Object.DontDestroyOnLoad(INSTANCE);
        }

        public static Coroutine StartCoroutine(IEnumerator routine)
        {
            if (routine == null)
            {
                return null;
            }

            return INSTANCE.StartCoroutine(routine);
        }

        public static void StopCoroutine(Coroutine coroutine)
        {
            if (coroutine == null)
            {
                return;
            }

            INSTANCE.StopCoroutine(coroutine);
        }
    }
}
